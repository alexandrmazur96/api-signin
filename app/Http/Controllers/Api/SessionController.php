<?php

namespace App\Http\Controllers\Api;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use PhpAmqpLib\Connection\AMQPConnection;
use Validator;

class SessionController extends Controller
{

    /**
     * Return data were stored in user-session (stored in memcached).
     */
    public function getSessionInfo(Request $request){
        $sessionId = $request->header('X-SESSION-ID');

        $cache = app('memcached');
        $session = $cache->get($sessionId);
        if (!$session) {
            return response('Session expire', 403);
        }
        return response(json_encode($session), 200);
    }

    /**
     * Check's user session and prolong it here
     */
    public function checkSession(Request $request)
    {
        $sessionId = $request->header('x-session-id');
        $cache = app('memcached');
        $session = $cache->get($sessionId);
        if (!$session) {
            return response('Session expire', 403);
        }

        $cache->set($sessionId, $session, Config::get('session.lifetime') * 60);
        return response('Session live', 200);
    }

    /**
     * Expire session by session-key
     */
    public function expire(Request $request)
    {
        $session_id = $request->header('x-session-id');

        if ($session_id === null) {
            return response('Authorization required', 401);
        }

        $cache = app('memcached');

        if ($cache->get($session_id)) {
            $cache->delete($session_id);
            return response('Session were expired', 200);
        } else {
            return response('Access forbidden', 403);
        }
    }

//    public function prolong(Request $request)
//    {
//        $session_id = $request->input('session_id');
//
//        if ($session_id === null) {
//            return response('Authorization required', 401);
//        }
//
//
//    }
}
