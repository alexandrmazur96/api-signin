<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Route::get('/test', function () {
    return view('welcome');
});

$router->group(['prefix' => '/v1/user'], function ($router) {
    $router->post('/auth', 'Api\SignInController@auth');
    $router->post('/request-password-reset', 'Api\SignInController@request_password_reset');
    $router->post('/set-password', 'Api\SignInController@set_password');
    $router->post('/set-password/by-token', 'Api\SignInController@set_password_by_token');
});


$router->group(['prefix' => '/v1/session'], function ($router) {
    $router->post('/check-session', 'Api\SessionController@checkSession');
    $router->post('/expire', 'Api\SessionController@expire');
});