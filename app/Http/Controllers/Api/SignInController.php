<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\User;
use App\Models\Email;
use App\Models\Password;
use App\Services\MailProducer;

/**
 * Проверяет авторизован-ли пользователь
 * Auth::check();
 *
 * Возвращает авторизованного пользователя или null
 * Auth::user();
 */

use Auth;

class SignInController extends Controller
{
    /**
     * Authorize user by login and password
     */
    public function auth(Request $request)
    {
        $requestParams = $request->only(
            'email',
            'password'
        );

        $requestValidationParams = [
            'email' => 'required|email|min:6',
            'password' => 'required|min:8'
        ];

        $requestValidator = Validator::make(
            $requestParams,
            $requestValidationParams
        );

        if ($requestValidator->fails()) {
            return response('Invalid params passed', 400);
        }

        $cache = app('memcached');

        if ($cache->get(User::generateSessionKey($requestParams['email']))) {
            return response('User logged', 302);
        }

        $email = Email::where('email', '=', $requestParams['email'])->first();
        if ($email === null) {
            return response('Access forbidden', 403);
        }

        $password = $email->password->password;

        if (Hash::check($requestParams['password'], $password)) {
            $user = User::find($email->user_id);
            $session_key = User::generateSessionKey($email->email);
            $cacheData = [
                'id' => $user->id,
                'email' => $email->email,
                'name_first' => $user->first_name,
                'name_last' => $user->last_name,
                'session_id' => $session_key,
                'session_expire' => Config::get('session.lifetime') * 60,
                'is_admin' => $user->is_admin,
                'is_author' => $user->is_author
            ];
            $cache->add($session_key, $cacheData, Config::get('session.lifetime') * 60);
            return response(json_encode($cacheData, JSON_UNESCAPED_UNICODE), 200);
        } else {
            return response('Access forbidden', 403);
        }
    }

    /**
     * Update user password
     */
    public function set_password(Request $request)
    {
        $requestParams = $request->only('password');
        $requestParams['session-id'] = $request->header('x-session-id');

        $requestPasswordValidation = [
            'password' => 'required|min:8'
        ];

        $requestTokenValidation = [
            'session-id' => 'required|min:32|max:32'
        ];

        $passwordValidator = Validator::make(
            $requestParams,
            $requestPasswordValidation
        );

        $tokenValidator = Validator::make(
            $requestParams,
            $requestTokenValidation
        );

        if ($passwordValidator->fails()) {
            return response('Invalid password value', 406); //406 - Not Acceptable
        }

        if ($tokenValidator->fails()) {
            return response('Token required', 401);
        }

        $cache = app('memcached');

        if ($cacheData = $cache->get($requestParams['session-id'])) {

            $password = Email::where('email', $cacheData['email'])->first()->password;
            $password->password = bcrypt($requestParams['password']);
            $password->save();

            return response('Password changed', 200);
        } else {
            return response('Access forbidden', 403);
        }
    }

    /**
     * Update user password by remind token
     */
    public function set_password_by_token(Request $request)
    {
        $requestParams = $request->only(
            'password-token',
            'password'
        );
        $requestValidation = [
            'password-token' => 'required|min:32|max:32',
            'password' => 'required|min:8'
        ];
        $requestValidator = Validator::make(
            $requestParams,
            $requestValidation
        );
        if ($requestValidator->fails()) {
            return response('Invalid params passed', 403);
        }

        $cache = app('memcached');

        if ($cacheData = $cache->get($requestParams['password-token'])) {
            $password = Email::where('email', '=', $cacheData)->first()->password;
            $password->password = bcrypt($requestParams['password']);
            $password->save();
            $cache->delete($requestParams['password-token']);
            return response('Password changed', 200);
        } else {
            return response('Invalid token were passed', 403);
        }
    }

    /**
     * Send remind email to user for reset password
     */
    public function request_password_reset(Request $request)
    {
        $requestParam = $request->only('email');
        $requestParamValidation = [
            'email' => 'required|email|min:6|unique:emails,email'
        ];
        $requestParamValidator = Validator::make(
            $requestParam,
            $requestParamValidation
        );

        if (!$requestParamValidator->fails()) {
            return response('Access forbidden', 403);
        }

        $cache = app('memcached');

        $passwordToken = Password::generatePasswordToken($requestParam['email']);

        $cache->add($passwordToken, $requestParam['email'], 3 * 60 * 60);

        $name = Email::where('email', $requestParam['email'])->first()->user->first_name;

        (new MailProducer())->produceRemindPassword($name, $requestParam['email'], $passwordToken);
        return response('Reset message was requested', 200);
    }

    /**
     * Check remind token if it's valid
     */
    public function checkRemindToken(Request $request)
    {

        $token = $request->input('remindToken');
        $cache = app('memcached');
        if ($cache->get($token) !== null)
            return response('Token correct', 200);
        else
            return response('Access forbidden', 403);
    }
}
