<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use Notifiable;

    protected $table = 'users';

    protected $guarded = ['id'];

    protected $appends = ['email', 'main'];

    protected $fillable = ['first_name', 'last_name', 'is_admin', 'is_author'];

    public function emails()
    {
        return $this->hasMany('App\Models\Email');
    }

    public function getEmailAttribute()
    {
        return $this->emails()
            ->where('user_id', '=', $this->id)
            ->where('main', '=', true)
            ->first()
            ->email;
    }

    public static function generateSessionKey($param1) : string{
        $key_phrase = $param1 . 'session key' . env('APP_KEY');
        return md5($key_phrase);
    }
}
