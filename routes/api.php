<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


$router->group(['prefix' => '/v1/user'], function ($router) {
    $router->post('/auth', 'Api\SignInController@auth');
    $router->post('/request-password-reset', 'Api\SignInController@request_password_reset');
    $router->post('/check-request-password-token', 'Api\SignInController@checkRemindToken');
    $router->post('/set-password', 'Api\SignInController@set_password');
    $router->post('/set-password/by-token', 'Api\SignInController@set_password_by_token');
});


$router->group(['prefix' => '/v1/session'], function ($router) {
    $router->post('/check-session', 'Api\SessionController@checkSession');
    $router->post('/expire', 'Api\SessionController@expire');
    $router->post('/getSessionInfo', 'Api\SessionController@getSessionInfo');
//    $router->post('/prolong', 'Api\SessionController@prolong');
});