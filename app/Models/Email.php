<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = "emails";
    protected $guarded = ['id'];
    protected $fillable = [
        'email',
        'is_main',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function password()
    {
        return $this->hasOne('App\Models\Password');
    }
}
