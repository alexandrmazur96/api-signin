<?php
/**
 * Created by PhpStorm.
 * User: ep1demic
 * Date: 15.08.17
 * Time: 17:27
 */

namespace App\Services;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class MailProducer
{
    /**
     * Отправляет данные регистрационного
     * письма новому пользователю в очередь RabbitMQ.
     *
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public function produceSignUp($name, $email, $password)
    {
        $connection = new AMQPStreamConnection(env('RABBIT_HOST'), env('RABBIT_PORT'), env('RABBIT_USER'), env('RABBIT_PASSWORD'));

        $channel = $connection->channel();

        $channel->queue_declare('mail', false, false, false, false);

        $msg = new AMQPMessage($this->makeJson($name, $email, 'signUp', $password));

        $channel->basic_publish($msg, '', 'mail');

        $channel->close();
        $connection->close();
    }

    /**
     * Send data for remind password for user to RabbitMQ queue
     *
     * @param string $name
     * @param string $email
     * @param string $remindToken
     */
    public function produceRemindPassword($name, $email, $remindToken)
    {
        $connection = new AMQPStreamConnection(env('RABBIT_HOST'), env('RABBIT_PORT'), env('RABBIT_USER'), env('RABBIT_PASSWORD'));

        $channel = $connection->channel();

        $channel->queue_declare('mail', false, false, false, false);

        $msg = new AMQPMessage($this->makeJson($name, $email, 'remindPassword', null, $remindToken));

        $channel->basic_publish($msg, '', 'mail');

        $channel->close();
        $connection->close();
    }

    /**
     * Generate json for send to rabbitmq
     *
     * @param string $name
     * @param string $email
     * @param string|null $password
     * @param string|null $remindToken
     * @return bool|string Json string or false when errors
     */
    private function makeJson($name, $email, $type, $password = null, $remindToken = null)
    {
        if ($password !== null) {
            return json_encode([
                'to' => $email,
                'type' => $type,
                'params' => [
                    'name' => $name,
                    'email' => $email,
                    'password' => $password
                ]
            ], JSON_UNESCAPED_UNICODE);
        } elseif ($remindToken !== null) {
            return json_encode([
                'to' => $email,
                'type' => $type,
                'params' => [
                    'name' => $name,
                    'email' => $email,
                    'remindToken' => $remindToken
                ]
            ], JSON_UNESCAPED_UNICODE);
        } elseif (($remindToken !== null && $password !== null)
            && ($remindToken === null && $password === null)) {
            return false;
        }
    }
}